# gridvis-client
[![NPM version](https://img.shields.io/npm/v/@janitza/gridvis-client.svg)](https://www.npmjs.com/package/@janitza/gridvis-client)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=janitza_gridvis-client&metric=coverage)](https://sonarcloud.io/dashboard?id=janitza_gridvis-client)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=janitza_gridvis-client&metric=alert_status)](https://sonarcloud.io/dashboard?id=janitza_gridvis-client)

A node js library to access the Janitza GridVis REST interface.

```typescript
import { GridVisClient } from '@janitza/gridvis-client';

async function main() {
    const client = new GridVisClient({url: ""});
    return await client.fetchGridVisVersion();
}

main().then((version) => {
    console.log(version)
});
```
