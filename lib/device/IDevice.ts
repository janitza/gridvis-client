export interface IDevice {
    connectionString?: string;
    description?: string;
    id: number;
    name: string;
    serialNr?: string;
    type: string;
}
