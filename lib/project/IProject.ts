export interface IProject {
    name: string;
    status: string;
    displayStatus: string;
    numberOfDevices?: number;
    path?: string;
}
